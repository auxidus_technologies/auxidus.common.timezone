﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Auxidus.Common.TimeZone
{
    public static class TimeZoneExtensions
    {
        public static void UseTimeZones(this IServiceCollection services)
        {
            services.AddScoped<UserCultureInfo>();
        }

        public static MvcOptions RegisterDateTimeProvider(this MvcOptions option, IServiceCollection serviceCollection)
        {
            if (option == null)
            {
                throw new ArgumentNullException(nameof(option));
            }
            option.ModelBinderProviders.Insert(0, new DateTimeBinderProvider(
              () => serviceCollection.BuildServiceProvider().GetService<UserCultureInfo>()
              ));
            return option;
        }

        public static MvcJsonOptions RegisterDateTimeConverter(this MvcJsonOptions option, IServiceCollection serviceCollection)
        {
            option.SerializerSettings.Converters.Add(
                new DateTimeConverter(() => serviceCollection.BuildServiceProvider().GetService<UserCultureInfo>())
                );
            return option;
        }
    }
}
