﻿using System;

namespace Auxidus.Common.TimeZone
{
    public class UserCultureInfo
    {
        public UserCultureInfo(string userTimeZone, string dateTimeFormat = "M/d/yyyy h:m:ss tt")
        {
            TimeZone = TimeZoneInfo.FindSystemTimeZoneById(userTimeZone);
            DateTimeFormat = dateTimeFormat;
        }
        public UserCultureInfo(TimeZoneInfo userTimeZone, string dateTimeFormat = "M/d/yyyy h:m:ss tt")
        {            
            TimeZone = userTimeZone;
            DateTimeFormat = dateTimeFormat;
        }
        
        public string DateTimeFormat { get; set; }

        public TimeZoneInfo TimeZone { get; set; }

        public DateTime GetUserLocalTime()
        {
            return TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZone);
        }

        public DateTime GetUtcTime(DateTime datetime)
        {
            return TimeZoneInfo.ConvertTime(datetime, TimeZone).ToUniversalTime();
        }
    }
}
