﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Auxidus.Common.TimeZone
{
    public class DateTimeConverter : DateTimeConverterBase
    {
        protected readonly Func<UserCultureInfo> UserCulture;

        public DateTimeConverter(Func<UserCultureInfo> userCulture)
        {
            UserCulture = userCulture;
        }
        
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime) || objectType == typeof(DateTime?);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            // Null are already filtered  
            var userCulture = UserCulture();
            writer.WriteValue(TimeZoneInfo.ConvertTime(Convert.ToDateTime(value), userCulture.TimeZone)
            .ToString(userCulture.DateTimeFormat));
            writer.Flush();
        }
    }
}
