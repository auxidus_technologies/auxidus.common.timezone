﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;

namespace Auxidus.Common.TimeZone
{
    public class DateTimeBinderProvider
    : IModelBinderProvider
    {

        protected readonly Func<UserCultureInfo> UserCulture;

        public DateTimeBinderProvider(Func<UserCultureInfo> userCulture)
        {
            UserCulture = userCulture;
        }

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            if (context.Metadata.UnderlyingOrModelType == typeof(DateTime))
            {
                return new DateTimeBinder(UserCulture());
            }
            return null;
        }
    }
}
