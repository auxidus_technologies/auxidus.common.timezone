﻿# How to Use

In service collection, add time zone services.

```
services.UseTimeZones();
```

When using MVC, pass in the custom formatter options.

```
 services.AddMvc(option =>  
         option.RegisterDateTimeProvider(services))  
       .AddJsonOptions(jsonOption =>  
         jsonOption.RegisterDateTimeConverter(services));  
```